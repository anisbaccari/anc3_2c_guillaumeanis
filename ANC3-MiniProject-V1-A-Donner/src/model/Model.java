package model;


import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;


/**
 *
 * @author 2711anbaccari
 */
public class Model {
    private int a ; 
    private List<String> ToDoList;
    private List<String> DoneList;
    
    private InvalidTransferException error;
    
    
    // a  faire SetDoTo(int a ),SetDoneList(int i); 
    //
    
    
    public Model(List<String> ls){
        ToDoList = ls.stream().distinct().filter(string -> string.length() > 2 ).
        		collect(Collectors.toList());
        this.DoneList=new ArrayList<>();
    
    }
    
    
    public Model( ){
    	this(new ArrayList<>());
     
    }
    
    
    public List<String> getToDoList(){
        return this.ToDoList;
    }
    
     public List<String> getDoneList(){
        return this.DoneList;
    }
     
     
    public void setToDo( int i )  {
    	
    	if( i >= 0)
    		this.ToDoList.remove(i);
    		
    		
    	if(this.ToDoList.get(i)== null) {
    		error.getMessage();
    	    }
    	
    	
    }
    
    public void setDone(int i ){
    	if( i >= 0) {
    		this.DoneList.remove(i);
    	}
    	else 
    		error.getMessage();
    	
    	
    }
    public boolean addToDo(String m){
        // if ( .. ) 
        this.ToDoList.add(m);
        return true;
        // else return false  
    }
    
}
