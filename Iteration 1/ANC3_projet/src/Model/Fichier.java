package Model;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.*;

public abstract class Fichier {

    public String nom;
    public String fName;
    public String fWay;
    public String fUp;
    public String fRoot;
    public String[] tablestate = { "Orphan", "Older", "New", "Same" };
    public String state;
    public String type;
    public long size;
    public FileTime fDate;
    public FileTime Lastmod;

    public Fichier() {
        this.state = tablestate[0];
        this.type = "d";

    }

    public Fichier(String n) {
        this.nom = n;

    }

    public Fichier(Path p) throws IOException {
    BasicFileAttributes attrs = Files.readAttributes(p, BasicFileAttributes.class);
    this.fName = p.getFileName().toString();
    this.fDate = attrs.creationTime();
    this.Lastmod =attrs.lastModifiedTime();
 }


 //////// SET //////////
public void setName( String n){
    this.nom=n;
}

public void setSize( long n){
    this.size=n;
}

public void setfName( String n){
    this.fName=n;
}
public void setfWay( String n){
    this.fWay=n;
}
public void setfUp( String n){
    this.fUp=n;
}
public void setfRoot( String n){
    this.fRoot=n;
}

public void setState(String n){
    this.state=n;
}

public void setType(String n){
    this.type=n;
}

////////////////////////
////// GET ////////
public String getfName(){
     return this.fName;
 }



 public String getState(){
    return this.state;
}

 public String getName(){
    return this.nom;
}
public String getfWay(){
    return this.fWay;
}
public String getfUp(){
    return this.fUp;
}
public String getfRoot(){
    return this.fRoot;
}

public FileTime getfDate(){
    return this.fDate;
}

public FileTime getlastMod(){
    return this.Lastmod;
}

public long getSize(){
    return this.size;
}
////////////////////


//// Abstract Methode //////
public abstract long size();
public abstract void ajout(File f);
public abstract Fichier copy(Fichier f); 
public abstract void supprime(Fichier f);

////////////////////////////

//////---------Comparateur/////////
@Override
public boolean equals(Object obj) {
    if (obj == null) {
        return false;
    }
    if (getClass() != obj.getClass()) {
        return false;
    }
    final Fichier other = (Fichier) obj;
    if (!Objects.equals(this.nom, other.nom)) {
        return false;
    }
    if (!Objects.equals(this.size, other.size)) {
        return false;
    }
   
    return true;
}

//////-------------------/////////

@Override
public String toString() {
     String str = this.fName+" -"+this.type+"-"+this.getfDate()+"-"+this.size+"-"+this.getState();
     return str;
}




}
